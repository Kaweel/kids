package com.kid.dev.controller;

import org.springframework.http.HttpStatus;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

public abstract class AbstractController {

    protected MultiValueMap< String, String> generateResponseHeader(){
        MultiValueMap< String, String> responseHeader = new LinkedMultiValueMap<>();
        responseHeader.add("httpStatus", HttpStatus.OK.toString());
        return responseHeader;
    }
}
