package com.kid.dev.controller;

import com.kid.dev.dao.bean.ErrorMsgBean;
import com.kid.dev.service.GetAllErrorMsgService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.List;

@RestController
@RequestMapping("errorMsg")
public class GetAllErrorMsgRestController extends AbstractController {

    @Autowired
    private GetAllErrorMsgService getAllErrorMsgService;

    @RequestMapping(value = "/getAllErrorMsg",method = RequestMethod.GET)
    public ResponseEntity<List<ErrorMsgBean>> execute(
            @RequestHeader(value = "appid") String appID,
            HttpServletRequest request,
            HttpServletResponse response,
            HttpSession session
    ){

        List<ErrorMsgBean> responseObj = getAllErrorMsgService.execute();

        return new ResponseEntity<>(responseObj, generateResponseHeader(), HttpStatus.OK);
    }

}
