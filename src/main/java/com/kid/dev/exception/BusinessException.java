package com.kid.dev.exception;

@SuppressWarnings("serial")
public class BusinessException extends RuntimeException {

    private int code;
    private String msg;

    public BusinessException(){
        super();
    }

    public BusinessException(int code,String msg){
        super(new Exception(msg));
        this.code = code;
        this.msg = msg;
    }

    public int getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }

}
