package com.kid.dev.exception;

import com.kid.dev.dao.ErrorMsgDao;
import com.kid.dev.dao.bean.ErrorMsgBean;
import com.kid.dev.util.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

@ControllerAdvice
public class KidsExceptionHandler {

    @Autowired
    ErrorMsgDao errorMsgDao;

    @ExceptionHandler({RuntimeException.class})
    @ResponseBody
    public ResponseEntity<ErrorMsgBean> runtimeException(RuntimeException e) {
        ErrorMsgBean responseObj = new ErrorMsgBean();
        responseObj.setErrorThMsg(Constants.INTERNAL_ERROR_MSG_EN);
        responseObj.setErrorEnMsg(Constants.INTERNAL_ERROR_MSG_TH);
        return new ResponseEntity<>(responseObj, HttpStatus.OK);
    }

    @ExceptionHandler({BusinessException.class})
    @ResponseBody
    public ResponseEntity<ErrorMsgBean> bussinessException(BusinessException be) {
        ErrorMsgBean errorMsgBean = errorMsgDao.getByErrorCode(be.getCode());
        ErrorMsgBean responseObj = new ErrorMsgBean();
        responseObj.setErrorThMsg(errorMsgBean.getErrorThMsg());
        responseObj.setErrorEnMsg(errorMsgBean.getErrorEnMsg());
        return new ResponseEntity<>(responseObj, HttpStatus.OK);
    }
}
