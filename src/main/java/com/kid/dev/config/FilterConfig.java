/*
package com.kid.dev.config;

import com.kid.dev.filter.KidsFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.servlet.Filter;

@Configuration
public class FilterConfig {

    @Autowired
    AutowireCapableBeanFactory beanFactory;

    // If u want to use
    // @Autowired with filter you must register you filter
    // with AutowireCapableBeanFactory

    @Bean
    public FilterRegistrationBean filterRegistrationBeanKids() {
        FilterRegistrationBean registration = new FilterRegistrationBean();
        Filter kidsFilter = new KidsFilter();
        beanFactory.autowireBean(kidsFilter);
        registration.setFilter(kidsFilter);
        registration.addUrlPatterns("/bankList/getAll");
        return registration;
    }

}
*/
