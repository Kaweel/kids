package com.kid.dev.aop;

import com.kid.dev.util.KidsUtil;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.joda.time.LocalDateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Aspect
public class LoggingAspect {

    /*Before Advice: it executes before a join point.
    After Returning Advice: it executes after a joint point completes normally.
    After Throwing Advice: it executes if method exits by throwing an exception.
    After (finally) Advice: it executes after a join point regardless of join point exit whether normally or exceptional return.
    Around Advice: It executes before and after a join point.*/


    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Pointcut("within(com.kid.dev.service.*)")
    public void
    loggingService() {}

  /*  @Before("loggingService()")
    public void loggingBeforeService(JoinPoint joinPoint) throws Throwable {
        //System.out.println("@Before Time : " + LocalDateTime.now());
        //System.out.println("@Before : " + KidsUtil.convertObjToJsonString(joinPoint.getArgs()));
    }


    @After("loggingService()")
    public void loggingAfterService(JoinPoint joinPoint) throws Throwable {
        //System.out.println("@After Time : " + LocalDateTime.now());
       // System.out.println("@After " + KidsUtil.convertObjToJsonString(joinPoint.getArgs()));
    }*/


    @Around("loggingService()")
    public Object
    loggingAroundService(ProceedingJoinPoint joinPoint) throws Throwable {

        //joinPoint.getSignature().getDeclaringType() -- > class com.kid.dev.service.GetAllBankServiceImpl
        //joinPoint.getSignature().getDeclaringTypeName() -- > com.kid.dev.service.GetAllBankServiceImpl
        //joinPoint.getSignature().getName() -- > execute

       /* System.out.println("@getDeclaringTypeName : " + joinPoint.getSignature().getDeclaringType());
        System.out.println("@getDeclaringTypeName : " + joinPoint.getSignature().getDeclaringTypeName());
        System.out.println("@Method Name : " + joinPoint.getSignature().getName());*/

        logger.info("Class {},Method Name {},Request Object{}",
                joinPoint.getSignature().getDeclaringTypeName(),
                joinPoint.getSignature().getName(),
                KidsUtil.convertObjToJsonString(joinPoint.getArgs()));

        /*System.out.println("@Request : " + KidsUtil.convertObjToJsonString(joinPoint.getArgs()));
        System.out.println("@Before Around : " + LocalDateTime.now());*/

        Object obj = joinPoint.proceed();/*
        System.out.println("@After Around : " + LocalDateTime.now());
        System.out.println("@Response : " + KidsUtil.convertObjToJsonString(joinPoint.getArgs()));*/

        logger.info("Class {},Method Name {},Response Object{}",
                joinPoint.getSignature().getDeclaringTypeName(),
                joinPoint.getSignature().getName(),
                KidsUtil.convertObjToJsonString(joinPoint.getArgs()));

        return obj;
    }

    /*@AfterReturning(pointcut = "loggingService()", returning = "val")
    public void loggingAfterReturningService(Object val) throws Throwable {
        logger.info("Response Object {}",KidsUtil.convertObjToJsonString(val));
    }*/


    @AfterThrowing(pointcut = "loggingService()", throwing = "e")
    public void
    loggingAfterThrowingService(JoinPoint joinPoint, Exception e) {
        logger.error(
                "Exception in {}.{}() with cause = {} and exception {}",
                joinPoint.getSignature().getDeclaringTypeName(),
                joinPoint.getSignature().getName(), e.getCause(), e
        );
    }

}
