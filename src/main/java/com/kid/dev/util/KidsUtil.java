package com.kid.dev.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.joda.time.LocalDateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.sql.Date;
import java.sql.Timestamp;

public class KidsUtil {

    public static String toStr(Object obj) {
        if (obj == null)
            return "";
        else
            return obj.toString();
    }

    public static int toInt(Object obj) {
        if (obj == null || toStr(obj).trim().isEmpty())
            return 0;
        else
            return Integer.parseInt(toStr(obj));
    }

    public static long toLong(Object obj) {
        if (obj == null || toStr(obj).trim().isEmpty())
            return 0;
        else
            return Long.parseLong(toStr(obj));
    }

    public static String localDateTimeToStr(LocalDateTime lcd,String formatDate){

        if(null == lcd)
            return null;

        DateTimeFormatter dtFormat = DateTimeFormat.forPattern(formatDate);
        return dtFormat.print(lcd);
    }

    public static String timeStampToStr(Timestamp ts,String formatDate){
        return localDateTimeToStr(sqlTimestampToJodaLocalDateTime(ts),formatDate);
    }

    public static Date jodatToSQLDate(LocalDateTime localDateTime) {
        return new Date(localDateTime.toDateTime().getMillis());
    }

    public static Timestamp jodaToSQLTimestamp(LocalDateTime localDateTime) {
        return new Timestamp(localDateTime.toDateTime().getMillis());
    }

    public static LocalDateTime sqlTimestampToJodaLocalDateTime(Timestamp timestamp) {

        if(null == timestamp)
            return null;
        else
            return LocalDateTime.fromDateFields(timestamp);
    }

    public static String dateTimeFormat(String str,String inputFormat,String outputFormat) {

        if(toStr(str).trim().isEmpty())
            return Constants.STRING_BLANK;

        if(toStr(inputFormat).trim().isEmpty())
            return Constants.STRING_BLANK;

        if(toStr(outputFormat).trim().isEmpty())
            return Constants.STRING_BLANK;

        DateTimeFormatter dtfInput = DateTimeFormat.forPattern(inputFormat);
        DateTimeFormatter dtfOutput = DateTimeFormat.forPattern(outputFormat);
        LocalDateTime lcd = dtfInput.parseLocalDateTime(str);
        return dtfOutput.print(lcd);
    }

    public static String convertObjToJsonString(Object data) throws Exception{
        ObjectMapper mapper = new ObjectMapper();
        return mapper.writeValueAsString(data);
    }

}
