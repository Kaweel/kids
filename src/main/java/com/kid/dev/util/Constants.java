package com.kid.dev.util;

public class Constants {

    public static final String STRING_BLANK = "";
    public static final String ddMMYYYYHHmmssS = "dd/MM/yyyy HH:mm:ss.S";
    public static final String yyyyMMddHHmmssS = "yyyy-MM-dd HH:mm:ss.S";
    public static final String ddMMMyyyyHHmmss = "dd MMM yyyy HH:mm:ss";
    public static final String ddMMMyyyyHHmm = "dd MMM yyyy HH:mm";
    public static final String ddMMMyyHHmm = "dd MMM yy HH:mm";

    public static final String INTERNAL_ERROR_MSG_EN = "Service is not available at this moment. Please try again later.";
    public static final String INTERNAL_ERROR_MSG_TH = "ระบบไม่สามารถใช้บริการได้ในขณะนี้ กรุณาลองใหม่ภายหลัง";
}
