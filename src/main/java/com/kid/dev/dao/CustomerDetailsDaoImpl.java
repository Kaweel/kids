package com.kid.dev.dao;

import com.kid.dev.dao.bean.CustomerDetailsBean;
import com.kid.dev.dao.rowmapper.CustomerDetailsRowMapper;
import com.kid.dev.dao.rowmapper.ErrorMsgRowMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class CustomerDetailsDaoImpl implements CustomerDetailsDao {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public CustomerDetailsBean getByUserName(String userName) {
        try{
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT * FROM CUSTOMER_DETAILS WHERE USER_NAME = ?");
            return this.jdbcTemplate.queryForObject(sql.toString(),new Object[] { userName }, new CustomerDetailsRowMapper());
        }catch (EmptyResultDataAccessException e) {
            return null;
        }
    }
}
