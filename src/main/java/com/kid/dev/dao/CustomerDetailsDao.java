package com.kid.dev.dao;

import com.kid.dev.dao.bean.CustomerDetailsBean;

public interface CustomerDetailsDao {

    CustomerDetailsBean getByUserName(String userName);

}
