package com.kid.dev.dao.rowmapper;

import com.kid.dev.dao.bean.ErrorMsgBean;
import com.kid.dev.util.KidsUtil;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ErrorMsgRowMapper implements RowMapper<ErrorMsgBean> {

    @Override
    public ErrorMsgBean mapRow(ResultSet rs, int rowNum) throws SQLException {
        ErrorMsgBean errorMsgBean = new ErrorMsgBean();
        errorMsgBean.setErrorCode(rs.getInt("ERROR_CODE"));
        errorMsgBean.setErrorThMsg(rs.getString("ERROR_TH_DESCRIPTION"));
        errorMsgBean.setErrorEnMsg(rs.getString("ERROR_EN_DESCRIPTION"));
        errorMsgBean.setCreateDate(KidsUtil.sqlTimestampToJodaLocalDateTime(rs.getTimestamp("CREATE_DATE")));
        errorMsgBean.setCreateBy(rs.getString("CREATE_BY"));
        errorMsgBean.setModifyDate(KidsUtil.sqlTimestampToJodaLocalDateTime(rs.getTimestamp("MODIFY_DATE")));
        errorMsgBean.setModifyBy(rs.getString("MODIFY_BY"));
        return errorMsgBean;
    }
}
