package com.kid.dev.dao.rowmapper;

import com.kid.dev.dao.bean.CustomerDetailsBean;
import com.kid.dev.util.KidsUtil;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class CustomerDetailsRowMapper implements RowMapper<CustomerDetailsBean> {

    @Override
    public CustomerDetailsBean mapRow(ResultSet rs, int rowNum) throws SQLException {
        CustomerDetailsBean customerDetailsBean = new CustomerDetailsBean();
        customerDetailsBean.setCustomerId(rs.getInt("CUSTOMER_ID"));
        customerDetailsBean.setIdentityCard(rs.getLong("IDENTITY_CARD"));
        customerDetailsBean.setTitle(rs.getString("TITLE"));
        customerDetailsBean.setFirstName(rs.getString("FIRST_NAME"));
        customerDetailsBean.setLastName(rs.getString("LAST_NAME"));
        customerDetailsBean.setEmailAddress(rs.getString("EMAIL_ADDRESS"));
        customerDetailsBean.setPhoneNumber(rs.getString("PHONE_NUMBER"));
        customerDetailsBean.setUserName(rs.getString("USER_NAME"));
        customerDetailsBean.setAuthCode(rs.getString("AUTH_CODE"));
        customerDetailsBean.setDisplayName(rs.getString("DISPLAY_NAME"));
        customerDetailsBean.setStatusId(rs.getInt("STATUS_ID"));
        customerDetailsBean.setRoleId(rs.getInt("ROLE_ID"));
        customerDetailsBean.setCreateDate(KidsUtil.sqlTimestampToJodaLocalDateTime(rs.getTimestamp("CREATE_DATE")));
        customerDetailsBean.setCreateBy(rs.getString("CREATE_BY"));
        customerDetailsBean.setModifyDate(KidsUtil.sqlTimestampToJodaLocalDateTime(rs.getTimestamp("MODIFY_DATE")));
        customerDetailsBean.setModifyBy(rs.getString("MODIFY_BY"));
        return customerDetailsBean;
    }
}
