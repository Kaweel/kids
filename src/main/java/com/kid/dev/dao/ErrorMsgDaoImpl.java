package com.kid.dev.dao;

import com.kid.dev.dao.bean.ErrorMsgBean;
import com.kid.dev.dao.rowmapper.ErrorMsgRowMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementSetter;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;

@Repository
public class ErrorMsgDaoImpl implements ErrorMsgDao{

    @Autowired
    private JdbcTemplate jdbcKids;

    public void setJdbcKids(JdbcTemplate jdbcKids){
        this.jdbcKids = jdbcKids;
    }

    @Override
    public void save(final ErrorMsgBean errorMsgBean) {
        StringBuilder sql = new StringBuilder();
        sql.append("INSERT INTO ERROR_MSG (ERROR_CODE,ERROR_TH_DESCRIPTION,ERROR_EN_DESCRIPTION,CREATE_DATE,CREATE_BY) VALUES (?,?,?,?,?)");
        this.jdbcKids.update(sql.toString(), new PreparedStatementSetter() {
            @Override
            public void setValues(PreparedStatement ps) throws SQLException {
                ps.setObject(1,errorMsgBean.getErrorCode());
                ps.setObject(2,errorMsgBean.getErrorThMsg());
                ps.setObject(3,errorMsgBean.getErrorEnMsg());
                ps.setObject(4,new Timestamp(errorMsgBean.getCreateDate().toDateTime().getMillis()));
                ps.setObject(5,errorMsgBean.getCreateBy());
            }
        });
    }

    @Override
    public void update(final ErrorMsgBean errorMsgBean) {
        StringBuilder sql = new StringBuilder();
        sql.append("UPDATE ERROR_MSG");
        sql.append(" SET ");
        sql.append("ERROR_TH_DESCRIPTION = ?, ");
        sql.append("ERROR_EN_DESCRIPTION = ?, ");
        sql.append("MODIFY_DATE = ?, ");
        sql.append("MODIFY_BY = ? ");
        sql.append("WHERE ERROR_CODE = ?");
        this.jdbcKids.update(sql.toString(), new PreparedStatementSetter() {
            @Override
            public void setValues(PreparedStatement ps) throws SQLException {
                ps.setObject(1,errorMsgBean.getErrorThMsg());
                ps.setObject(2,errorMsgBean.getErrorEnMsg());
                ps.setObject(3,new Timestamp(errorMsgBean.getModifyDate().toDateTime().getMillis()));
                ps.setObject(4,errorMsgBean.getModifyBy());
                ps.setObject(5,errorMsgBean.getErrorCode());
            }
        });
    }

    @Override
    public void delete(final int errorCode) {
        StringBuilder sql = new StringBuilder();
        sql.append("DELETE FROM ERROR_MSG WHERE ERROR_CODE = ?");
        this.jdbcKids.update(sql.toString(), new PreparedStatementSetter() {
            @Override
            public void setValues(PreparedStatement ps) throws SQLException {
                ps.setObject(1,errorCode);
            }
        });
    }

    @Override
    public List<ErrorMsgBean> getAllErrorMsg() {
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT * FROM ERROR_MSG");
        return this.jdbcKids.query(sql.toString(), new ErrorMsgRowMapper());
    }

    @Override
    public ErrorMsgBean getByErrorCode(final int errorCode) {
        try{
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT * FROM ERROR_MSG WHERE ERROR_CODE = ?");
            return this.jdbcKids.queryForObject(sql.toString(),new Object[] { errorCode }, new ErrorMsgRowMapper());
        }catch (EmptyResultDataAccessException e) {
            return null;
        }
    }
}
