package com.kid.dev.dao.bean;

import java.io.Serializable;

public class ErrorMsgBean extends DefaultBean implements Serializable {

    private static final long serialVersionUID = 3777121571584292947L;

    int errorCode;
    String errorThMsg;
    String errorEnMsg;

    public int getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorThMsg() {
        return errorThMsg;
    }

    public void setErrorThMsg(String errorThMsg) {
        this.errorThMsg = errorThMsg;
    }

    public String getErrorEnMsg() {
        return errorEnMsg;
    }

    public void setErrorEnMsg(String errorEnMsg) {
        this.errorEnMsg = errorEnMsg;
    }
}
