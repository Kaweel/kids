package com.kid.dev.dao.bean;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.kid.dev.util.Constants;
import com.kid.dev.util.KidsUtil;
import org.joda.time.LocalDateTime;

import java.io.Serializable;

public class DefaultBean implements Serializable {

    private static final long serialVersionUID = -1256397950133143348L;

    @JsonIgnore
    LocalDateTime createDate;

    @JsonIgnore
    LocalDateTime modifyDate;

    String createBy;
    String modifyBy;

    public LocalDateTime getCreateDate() {
        return createDate;
    }

    public void setCreateDate(LocalDateTime createDate) {
        this.createDate = createDate;
    }

    public LocalDateTime getModifyDate() {
        return modifyDate;
    }

    public void setModifyDate(LocalDateTime modifyDate) {
        this.modifyDate = modifyDate;
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    public String getModifyBy() {
        return modifyBy;
    }

    public void setModifyBy(String modifyBy) {
        this.modifyBy = modifyBy;
    }

    public String getCreateDateFormat(){
        return KidsUtil.localDateTimeToStr(this.createDate, Constants.ddMMMyyyyHHmmss);
    }

    public String getModifyDateFormat(){
        return KidsUtil.localDateTimeToStr(this.modifyDate, Constants.ddMMMyyyyHHmmss);
    }

}
