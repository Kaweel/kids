package com.kid.dev.dao;

import com.kid.dev.dao.bean.ErrorMsgBean;

import java.util.List;

public interface ErrorMsgDao {

    void save(ErrorMsgBean errorMsgBean);

    void update(ErrorMsgBean errorMsgBean);

    void delete(int errorCode);

    List<ErrorMsgBean> getAllErrorMsg();

    ErrorMsgBean getByErrorCode(int errorCode);

}
