/*
package com.kid.dev.service;

import com.kid.dev.dao.CustomerDetailsDao;
import com.kid.dev.dao.bean.CustomerDetailsBean;
import com.kid.dev.exception.BusinessException;
import com.kid.dev.util.KidsUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserDetailServiceImpl implements UserDetailsService{

    @Autowired
    private CustomerDetailsDao customerDetailsDao;

    @Override
    public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {

        CustomerDetailsBean customerDetailsBean = customerDetailsDao.getByUserName(userName);
        if(null == customerDetailsBean)
            throw new BusinessException(1000,"CustomerDetailsBean is null");

        return new org.springframework.security.core.userdetails.User(
                customerDetailsBean.getUserName(),
                customerDetailsBean.getAuthCode(),
                true, true, true, true,
                getGrantedAuthorities(customerDetailsBean));
    }

    private List<GrantedAuthority> getGrantedAuthorities(CustomerDetailsBean customerDetailsBean){
        List<GrantedAuthority> authorities = new ArrayList<>();
        authorities.add(new SimpleGrantedAuthority(KidsUtil.toStr(customerDetailsBean.getRoleId())));
        return authorities;
    }

}
*/
