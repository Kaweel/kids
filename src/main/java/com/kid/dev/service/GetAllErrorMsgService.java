package com.kid.dev.service;

import com.kid.dev.dao.bean.ErrorMsgBean;

import java.util.List;

public interface GetAllErrorMsgService {

    List<ErrorMsgBean> execute();

}
