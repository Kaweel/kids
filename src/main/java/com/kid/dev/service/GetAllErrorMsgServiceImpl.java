package com.kid.dev.service;

import com.kid.dev.dao.ErrorMsgDao;
import com.kid.dev.dao.bean.ErrorMsgBean;
import com.kid.dev.exception.BusinessException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class GetAllErrorMsgServiceImpl implements GetAllErrorMsgService {

    @Autowired
    private ErrorMsgDao errorMsgDao;

    @Override
    @Transactional
    public List<ErrorMsgBean> execute() {

        List<ErrorMsgBean> errorMsgBeanList = errorMsgDao.getAllErrorMsg();
        if(errorMsgBeanList.isEmpty())
            throw new BusinessException(10001,"errorMsg list is empty");

        return errorMsgBeanList;
    }
}
