package com.kid.dev.dao;

import com.kid.dev.catalog.Iteration001;
import com.kid.dev.dao.bean.ErrorMsgBean;
import org.joda.time.LocalDateTime;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabase;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;

import java.sql.SQLException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

@Category(Iteration001.class)
public class ErrorMsgDaoImplTest {

    EmbeddedDatabase db;

    @Before
    public void
    setSchema() throws SQLException {
        EmbeddedDatabaseBuilder builder = new EmbeddedDatabaseBuilder();
        db = builder.setType(EmbeddedDatabaseType.H2)
                .addScript("classpath:table/create_table_error_msg.sql")
                .addScript("classpath:test_data/insert_error_msg_data.sql")
                .build();
    }

    @After
    public void releasedResources() {
        db.shutdown();
    }

    @Test
    public void
    get_all(){
        JdbcTemplate jdbcTemplate = new JdbcTemplate(db);
        ErrorMsgDaoImpl errorMsgDao = new ErrorMsgDaoImpl();
        errorMsgDao.setJdbcKids(jdbcTemplate);
        int size = errorMsgDao.getAllErrorMsg().size();
        assertEquals(2,size);
    }

    @Test
    public void
    get_error_msg_found_data(){
        JdbcTemplate jdbcTemplate = new JdbcTemplate(db);
        ErrorMsgDaoImpl errorMsgDao = new ErrorMsgDaoImpl();
        errorMsgDao.setJdbcKids(jdbcTemplate);
        ErrorMsgBean errorMsgBean = errorMsgDao.getByErrorCode(1000);
        assertEquals(1000,errorMsgBean.getErrorCode());
    }

    @Test
    public void
    get_error_msg_not_found_data(){
        JdbcTemplate jdbcTemplate = new JdbcTemplate(db);
        ErrorMsgDaoImpl errorMsgDao = new ErrorMsgDaoImpl();
        errorMsgDao.setJdbcKids(jdbcTemplate);
        ErrorMsgBean errorMsgBean = errorMsgDao.getByErrorCode(1999);
        assertNull(errorMsgBean);
    }

    @Test
    public void
    save_success(){
        JdbcTemplate jdbcTemplate = new JdbcTemplate(db);
        ErrorMsgDaoImpl errorMsgDao = new ErrorMsgDaoImpl();
        errorMsgDao.setJdbcKids(jdbcTemplate);
        int before = errorMsgDao.getAllErrorMsg().size();
        ErrorMsgBean errorMsgBean = new ErrorMsgBean();
        errorMsgBean.setErrorCode(1200);
        errorMsgBean.setErrorThMsg("ข้อความภาษาไทย");
        errorMsgBean.setErrorEnMsg("Error Message");
        errorMsgBean.setCreateDate(LocalDateTime.now());
        errorMsgBean.setCreateBy("Kids Developer");
        errorMsgDao.save(errorMsgBean);
        int after = errorMsgDao.getAllErrorMsg().size();
        assertEquals(before+1,after);
    }

    @Test
    public void
    update_success(){
        JdbcTemplate jdbcTemplate = new JdbcTemplate(db);
        ErrorMsgDaoImpl errorMsgDao = new ErrorMsgDaoImpl();
        errorMsgDao.setJdbcKids(jdbcTemplate);
        ErrorMsgBean errorMsgBean = errorMsgDao.getByErrorCode(1000);
        errorMsgBean.setErrorEnMsg("CustomerDetailsBean");
        errorMsgBean.setErrorThMsg("ไทย");
        errorMsgBean.setModifyDate(LocalDateTime.now());
        errorMsgBean.setModifyBy("Kawee");
        errorMsgDao.update(errorMsgBean);
        ErrorMsgBean errorMsgBeanNew = errorMsgDao.getByErrorCode(1000);
        assertEquals(errorMsgBean.getErrorEnMsg(),errorMsgBeanNew.getErrorEnMsg());
        assertEquals(errorMsgBean.getErrorThMsg(),errorMsgBeanNew.getErrorThMsg());
        assertEquals(errorMsgBean.getModifyBy(),errorMsgBeanNew.getModifyBy());
    }

    @Test
    public void
    delete_success(){
        JdbcTemplate jdbcTemplate = new JdbcTemplate(db);
        ErrorMsgDaoImpl errorMsgDao = new ErrorMsgDaoImpl();
        errorMsgDao.setJdbcKids(jdbcTemplate);
        int before = errorMsgDao.getAllErrorMsg().size();
        errorMsgDao.delete(1000);
        int after = errorMsgDao.getAllErrorMsg().size();
        assertEquals(before-1,after);
    }

}
