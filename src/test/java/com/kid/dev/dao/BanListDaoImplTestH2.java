/*
package com.kid.dev.dao;

import com.kid.dev.config.DataSourceTestConfig;
import com.kid.dev.config.H2DatabaseTestConfig;
import org.h2.tools.RunScript;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.PropertySource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.nio.charset.Charset;
import java.sql.SQLException;

import static com.kid.dev.config.H2DatabaseConstant.*;
import static org.junit.Assert.assertEquals;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = DataSourceTestConfig.class)
@EnableAutoConfiguration(exclude={DataSourceAutoConfiguration.class})
@PropertySource(value = {"classpath:application-dev.properties"})
@ComponentScan("com.kid.dev")
@Transactional(value = "txKids")
public class BanListDaoImplTest extends H2DatabaseTestConfig {

    @Autowired
    BankDao bankDao;

    @BeforeClass
    public static void setSchema() throws SQLException {
        RunScript.execute(JDBC_URL,USER,PASSWORD,"src/test/resources/table/BankList.sql", Charset.forName("UTF8"),false);
    }

    public BanListDaoImplTest(){
        initialTestData("src/test/resources/test_data/BankList.xml");
    }

    @Test
    public void
    get_allBankList(){
        int size = bankDao.getAllBankList().size();
        assertEquals(13,size);
    }

}
*/
