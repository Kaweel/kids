package com.kid.dev.config;


import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;

import javax.sql.DataSource;

@Configuration
public class DataSourceTestConfig {

    @Primary
    @Bean(name="dsKids")
    @ConfigurationProperties(prefix="spring.datasource.kids")
    public DataSource majorDataSource() {
        return DataSourceBuilder.create().build();
    }

    @Bean(name = "jdbcKids")
    public JdbcTemplate majorJdbcTemplate(@Qualifier("dsKids") DataSource dataSource) {
        return new JdbcTemplate(dataSource);
    }

    @Bean(name = "txKids")
    public DataSourceTransactionManager majorDataSourceTransactionManager(@Qualifier("dsKids") DataSource dataSource){
        DataSourceTransactionManager txManager = new DataSourceTransactionManager();
        txManager.setDataSource(dataSource);
        return txManager;
    }

}
