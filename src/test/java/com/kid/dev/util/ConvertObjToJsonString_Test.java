package com.kid.dev.util;

import com.kid.dev.catalog.Iteration001;
import org.joda.time.LocalDateTime;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

@Category(Iteration001.class)
public class ConvertObjToJsonString_Test {

    @Test
    public void convertNull() throws Exception {
        String actual = KidsUtil.convertObjToJsonString(null);
        String excepted = "null";
        assertEquals(excepted,actual);
    }

    /*@Test
    public void convertEmptyObj() throws Exception {
        String actual = KidsUtil.convertObjToJsonString(new BankBean());
        StringBuilder expected = new StringBuilder();
        expected.append(
                "{" +
                    "\"createBy\":null," +
                    "\"modifyBy\":null," +
                    "\"bankCode\":null," +
                    "\"thBankName\":null," +
                    "\"enBankName\":null," +
                    "\"abbreviation\":null," +
                    "\"bankFIID\":null," +
                    "\"bankTaxID\":null," +
                    "\"serviceType\":null," +
                    "\"serviceActiveFlag\":null," +
                    "\"modifyDateFormat\":null,"+
                    "\"createDateFormat\":null}"
        );
        assertEquals(expected.toString(),actual);
    }*/

    /*@Test
    public void convertEmptyListObj() throws Exception {
        List<BankBean> thaiBankList = new ArrayList<>();
        String actual = KidsUtil.convertObjToJsonString(thaiBankList);
        String excepted = "[]";
        assertEquals(excepted,actual);
    }*/

    /*@Test
    public void convertObj() throws Exception {

        BankBean objKbank = new BankBean();
        objKbank.setBankCode("004");
        objKbank.setThBankName("กสิกรไทย");
        objKbank.setEnBankName("KASIKORNBANK");
        objKbank.setAbbreviation("KBank");
        objKbank.setBankFIID("KBNK");
        objKbank.setBankTaxID("1234567890");
        objKbank.setServiceActiveFlag("O");
        objKbank.setServiceType("Y");
        objKbank.setCreateDate(new LocalDateTime("2016-10-23T00:00:00.000"));
        objKbank.setCreateBy("Kids Develop");

        String actual = KidsUtil.convertObjToJsonString(objKbank);

        StringBuilder expected = new StringBuilder();
        expected.append(
                "{" +
                    "\"createBy\":\"Kids Develop\"," +
                    "\"modifyBy\":null," +
                    "\"bankCode\":\"004\"," +
                    "\"thBankName\":\"กสิกรไทย\"," +
                    "\"enBankName\":\"KASIKORNBANK\"," +
                    "\"abbreviation\":\"KBank\"," +
                    "\"bankFIID\":\"KBNK\"," +
                    "\"bankTaxID\":\"1234567890\"," +
                    "\"serviceType\":\"Y\"," +
                    "\"serviceActiveFlag\":\"O\"," +
                    "\"modifyDateFormat\":null,"+
                    "\"createDateFormat\":\"23 Oct 2016 00:00:00\"}"
        );
        assertEquals(expected.toString(),actual);
    }

    @Test
    public void convertListObj() throws Exception {

        BankBean objKbank = new BankBean();
        objKbank.setBankCode("004");
        objKbank.setThBankName("กสิกรไทย");
        objKbank.setEnBankName("KASIKORNBANK");
        objKbank.setAbbreviation("KBank");
        objKbank.setBankFIID("KBNK");
        objKbank.setBankTaxID("1234567890");
        objKbank.setServiceActiveFlag("O");
        objKbank.setServiceType("Y");
        objKbank.setCreateDate(new LocalDateTime("2016-10-23T00:00:00.000"));
        objKbank.setCreateBy("Kids Develop");

        BankBean objTMB = new BankBean();
        objTMB.setBankCode("011");
        objTMB.setThBankName("ทหารไทย");
        objTMB.setEnBankName("TMB Bank");
        objTMB.setAbbreviation("TMB");
        objTMB.setBankFIID("TMBA");
        objTMB.setBankTaxID("3101077544");
        objTMB.setServiceActiveFlag("O");
        objTMB.setServiceType("Y");
        objTMB.setCreateDate(new LocalDateTime("2016-10-23T00:00:00.000"));
        objTMB.setCreateBy("Kids Develop");

        BankBean objGSB = new BankBean();
        objGSB.setBankCode("030");
        objGSB.setThBankName("ออมสิน");
        objGSB.setEnBankName("Government Savings Bank");
        objGSB.setAbbreviation("GSB");
        objGSB.setBankFIID("GSBA");
        objGSB.setBankTaxID("3101048315");
        objGSB.setServiceActiveFlag("O");
        objGSB.setServiceType("Y");
        objGSB.setCreateDate(new LocalDateTime("2016-10-23T00:00:00.000"));
        objGSB.setCreateBy("Kids Develop");

        List<BankBean> thaiBankList = new ArrayList<>();
        thaiBankList.add(objKbank);
        thaiBankList.add(objTMB);
        thaiBankList.add(objGSB);

        String actual = KidsUtil.convertObjToJsonString(thaiBankList);

        StringBuilder expected = new StringBuilder();
        expected.append("[" +
                "{" +
                    "\"createBy\":\"Kids Develop\"," +
                    "\"modifyBy\":null," +
                    "\"bankCode\":\"004\"," +
                    "\"thBankName\":\"กสิกรไทย\"," +
                    "\"enBankName\":\"KASIKORNBANK\"," +
                    "\"abbreviation\":\"KBank\"," +
                    "\"bankFIID\":\"KBNK\"," +
                    "\"bankTaxID\":\"1234567890\"," +
                    "\"serviceType\":\"Y\"," +
                    "\"serviceActiveFlag\":\"O\"," +
                    "\"modifyDateFormat\":null,"+
                    "\"createDateFormat\":\"23 Oct 2016 00:00:00\""+
                "},{" +
                    "\"createBy\":\"Kids Develop\"," +
                    "\"modifyBy\":null," +
                    "\"bankCode\":\"011\"," +
                    "\"thBankName\":\"ทหารไทย\"," +
                    "\"enBankName\":\"TMB Bank\"," +
                    "\"abbreviation\":\"TMB\"," +
                    "\"bankFIID\":\"TMBA\"," +
                    "\"bankTaxID\":\"3101077544\"," +
                    "\"serviceType\":\"Y\"," +
                    "\"serviceActiveFlag\":\"O\"," +
                    "\"modifyDateFormat\":null,"+
                    "\"createDateFormat\":\"23 Oct 2016 00:00:00\""+
                "},{" +
                    "\"createBy\":\"Kids Develop\"," +
                    "\"modifyBy\":null," +
                    "\"bankCode\":\"030\"," +
                    "\"thBankName\":\"ออมสิน\"," +
                    "\"enBankName\":\"Government Savings Bank\"," +
                    "\"abbreviation\":\"GSB\"," +
                    "\"bankFIID\":\"GSBA\"," +
                    "\"bankTaxID\":\"3101048315\"," +
                    "\"serviceType\":\"Y\"," +
                    "\"serviceActiveFlag\":\"O\"," +
                    "\"modifyDateFormat\":null," +
                    "\"createDateFormat\":\"23 Oct 2016 00:00:00\""+
                "}" +
        "]");

        assertEquals(expected.toString(),actual);
    }*/
    @Test
    public void getCurrnetTimeStamp(){
        LocalDateTime localDateTime = new LocalDateTime();
        System.out.println("LocalDateTime : " + localDateTime);
        System.out.println("LocalDateTime to SQL Date : " + KidsUtil.jodatToSQLDate(localDateTime));
        Timestamp timestamp = KidsUtil.jodaToSQLTimestamp(localDateTime);
        System.out.println("LocalDateTime to SQL Timestamp : " + timestamp);
        System.out.println("SQL Timestamp to LocalDateTime : " + KidsUtil.sqlTimestampToJodaLocalDateTime(timestamp));
    }

}
