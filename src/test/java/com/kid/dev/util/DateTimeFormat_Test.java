package com.kid.dev.util;

import com.kid.dev.catalog.Iteration001;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import static org.junit.Assert.assertEquals;

@Category(Iteration001.class)
public class DateTimeFormat_Test {

    @Test
    public void input_null() {

        String actual = KidsUtil.dateTimeFormat(null,null,null);
        String expected = "";

        assertEquals(expected,actual);
    }

    @Test
    public void input_empty() {

        String actual = KidsUtil.dateTimeFormat(Constants.STRING_BLANK,Constants.STRING_BLANK,Constants.STRING_BLANK);
        String expected = Constants.STRING_BLANK;

        assertEquals(expected,actual);
    }

    @Test
    public void input_yyyyMMddHHmmssS_output_ddMMMyyyyHHmmss() {

        String actual = KidsUtil.dateTimeFormat("2016-09-06 00:00:00.0",Constants.yyyyMMddHHmmssS,Constants.ddMMMyyyyHHmmss);
        String expected = "06 Sep 2016 00:00:00";
        assertEquals(expected,actual);
    }

    @Test
    public void input_ddMMYYYYHHmmssS_output_ddMMMyyyyHHmmss() {

        String actual = KidsUtil.dateTimeFormat("06/07/2016 23:59:59.0",Constants.ddMMYYYYHHmmssS,Constants.ddMMMyyyyHHmmss);
        String expected = "06 Jul 2016 23:59:59";
        assertEquals(expected,actual);
    }

    @Test
    public void input_ddMMYYYYHHmmssS_output_ddMMMyyyyHHmm() {

        String actual = KidsUtil.dateTimeFormat("06/10/2016 23:59:59.0",Constants.ddMMYYYYHHmmssS,Constants.ddMMMyyyyHHmm);
        String expected = "06 Oct 2016 23:59";
        assertEquals(expected,actual);
    }
    @Test
    public void input_ddMMYYYYHHmmssS_output_ddMMMyyHHmm() {

        String actual = KidsUtil.dateTimeFormat("06/12/2016 23:59:59.0",Constants.ddMMYYYYHHmmssS,Constants.ddMMMyyHHmm);
        String expected = "06 Dec 16 23:59";
        assertEquals(expected,actual);
    }

}
