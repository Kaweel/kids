package com.kid.dev.util;


import com.kid.dev.catalog.Iteration001;
import org.abstractj.kalium.crypto.Box;
import org.abstractj.kalium.crypto.Random;
import org.abstractj.kalium.keys.KeyPair;
import org.abstractj.kalium.keys.PrivateKey;
import org.abstractj.kalium.keys.PublicKey;
import org.joda.time.LocalDateTime;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.springframework.util.Base64Utils;

import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;

import static org.junit.Assert.assertEquals;

@Category(Iteration001.class)
public class ToStr_Test {

    @Test
    public void toStr_obj_is_null() {
        String actual = KidsUtil.toStr(null);
        String excepted = "";
        assertEquals(excepted,actual);
    }

    @Test
    public void toStr_obj_is_blank() {
        String actual = KidsUtil.toStr("");
        String excepted = "";
        assertEquals(excepted,actual);
    }

    @Test
    public void toStr_obj_is_int() {
        String actual = KidsUtil.toStr(15);
        String excepted = "15";
        assertEquals(excepted,actual);
    }

    @Test
    public void toStr_obj_is_long() {
        String actual = KidsUtil.toStr((long) 10);
        String excepted = "10";
        assertEquals(excepted,actual);
    }

    @Test
    public void toStr_obj_is_BigDecimal() {
        String actual = KidsUtil.toStr(new BigDecimal("100261760.2512"));
        String excepted = "100261760.2512";
        assertEquals(excepted,actual);
    }

    @Test
    public void toStr_obj_is_LocalDateTIme() {
        String actual = KidsUtil.toStr(LocalDateTime.now().withTime(0,0,0,0));
        String excepted = LocalDateTime.now().withTime(0,0,0,0).toString();
        assertEquals(excepted,actual);
    }

    @Test
    public void kalium() throws UnsupportedEncodingException {

        KeyPair kaweeKeyPair = new KeyPair();
        PublicKey kaweePublicKey = kaweeKeyPair.getPublicKey();
        PrivateKey kaweePrivateKey = kaweeKeyPair.getPrivateKey();

        KeyPair idaKeyPair = new KeyPair();
        PublicKey idaPublicKey = idaKeyPair.getPublicKey();
        PrivateKey idaPrivateKey = idaKeyPair.getPrivateKey();

        Box idabox = new Box(kaweePublicKey,idaPrivateKey);
        Box kaweebox = new Box(idaPublicKey,kaweePrivateKey);

        byte [] nonce = new Random().randomBytes(24);

        String msgIda = "Hello";
        byte [] ciphertextIda = idabox.encrypt(nonce,msgIda.getBytes("UTF-8"));
        String resultIda = new String(kaweebox.decrypt(nonce,ciphertextIda));

        String msgKawee = "Hello Ida";
        byte [] ciphertextKawee = kaweebox.encrypt(nonce,resultIda.getBytes("UTF-8"));
        String reultKawee = new String(idabox.decrypt(nonce,ciphertextKawee));

        assertEquals(msgIda,resultIda);
        assertEquals(msgKawee,reultKawee);
    }

}
