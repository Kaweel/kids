package com.kid.dev.service;

import com.kid.dev.catalog.Iteration001;
import com.kid.dev.dao.ErrorMsgDao;
import com.kid.dev.dao.bean.ErrorMsgBean;
import com.kid.dev.exception.BusinessException;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

@Category(Iteration001.class)
public class GetAllErrorMsgServiceTest {

    @Mock
    private ErrorMsgDao errorMsgDao;

    private GetAllErrorMsgServiceImpl service;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        service = new GetAllErrorMsgServiceImpl();
        ReflectionTestUtils.setField(service, "errorMsgDao", errorMsgDao);
    }

    @Test(expected = BusinessException.class)
    public void
    GetAllErrorMsg_not_found_data(){
        List<ErrorMsgBean> errorMsgBeanList = new ArrayList<>();
        Mockito.when(errorMsgDao.getAllErrorMsg()).thenReturn(errorMsgBeanList);
        service.execute();
    }

    @Test
    public void
    GetAllErrorMsg_found_data(){
        List<ErrorMsgBean> errorMsgBeanList = new ArrayList<>();
        ErrorMsgBean errorMsgBean = new ErrorMsgBean();
        errorMsgBean.setErrorCode(1002);
        errorMsgBean.setErrorThMsg("ทดสอบข้อความ");
        errorMsgBean.setErrorEnMsg("test msg");
        errorMsgBeanList.add(errorMsgBean);
        Mockito.when(errorMsgDao.getAllErrorMsg()).thenReturn(errorMsgBeanList);
        List<ErrorMsgBean> result = service.execute();
        assertEquals(1,result.size());
    }

}
