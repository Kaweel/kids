CREATE TABLE BankList(
	BankCode char(3) PRIMARY KEY NOT NULL,
	ThBankName varchar(40) NOT NULL,
	EnBankName varchar(40) NOT NULL,
	Abbreviation varchar(10) NOT NULL,
	BankFIID char(4) NULL,
	BankTaxID varchar(15) NULL,
	ServiceType char(1) NULL,
	ServiceActiveFlag char(1) NULL,
	CreateDate datetime NOT NULL,
	CreateBy varchar(20) NOT NULL,
	ModifyDate datetime NULL,
	ModifyBy varchar(20) NULL
);